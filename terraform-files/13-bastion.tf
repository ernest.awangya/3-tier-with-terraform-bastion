resource "aws_instance" "ec2_instance" {
  ami                    = var.image-id
  instance_type          = var.instance-type
  key_name               = var.key-name
  vpc_security_group_ids = [aws_security_group.asg-security-group-web.id]
  subnet_id = aws_subnet.web-subnet2.id

 connection {
    type = "ssh"
    host = aws_instance.ec2_instance.public_ip
    user = "ec2-user"
    private_key = file("/Users/eawangya/Downloads/devops-key2.pem")
  }
  
  provisioner "file" {
    source = "/Users/eawangya/Downloads/devops-key2.pem"
    destination = "/tmp/devops-key2.pem"
  }

  provisioner "remote-exec" {
    inline = [ 
        "sudo chmod 400 /tmp/devops-key2.pem",
     ]

  }


  tags = {
    "Name" = "bastion-host"
  }
}