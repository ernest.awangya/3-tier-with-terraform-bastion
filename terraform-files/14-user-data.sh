#!/bin/bash
# Use this for your user data (script from top to bottom)
# install httpd (Linux 2 version)
yum update -y
yum install -y httpd
systemctl start httpd
systemctl enable httpd
cat <<EOF > /var/www/html/index.html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hello World Page</title>
</head>
<body>
    <header>
        <h1>Server Name: $(hostname -f)</h1>
    </header>
    <section>
        <p>This is a simple webpage generated running in a 3-tier archiecture.</p>
    </section>
    <footer>
        <p>Generated on $(date) by Ernest Awangya</p>
    </footer>
</body>
</html>
EOF