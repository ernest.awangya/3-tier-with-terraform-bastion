# data "aws_secretsmanager_secret_version" "database_credentials" {
#   secret_id = aws_secretsmanager_secret.database_credentials.id
# }

resource "aws_db_instance" "rds-db" {
  allocated_storage      = 10
  db_name                = var.db-name
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = var.instance-class
  storage_encrypted      = true
  #kms_key_id = aws_secretsmanager_secret.database_credentials.arn
  username               = var.db-username
  password               = var.db-password
  # username               = jsondecode(data.aws_secretsmanager_secret_version.database_credentials.secret_string)["username"]
  # password               = jsondecode(data.aws_secretsmanager_secret_version.database_credentials.secret_string)["password"]
  parameter_group_name   = "default.mysql5.7"
  multi_az               = true
  db_subnet_group_name   = aws_db_subnet_group.subnet-grp.name
  vpc_security_group_ids = [aws_security_group.db-sg.id]
  skip_final_snapshot    = true
}