output "web-server-dns" {
  value = aws_lb.alb-app.dns_name
}

output "rds_hostname" {
  value = aws_db_instance.rds-db.endpoint
}